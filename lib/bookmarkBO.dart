library bible.bookmark_bo;

class BookmarkBO {
  int bmId;
  String bmCurrent;
  String bmDesc;
  String bmDev1;
  String bmDev2;
  String bmDev3;
  String bmPrev1;
  String bmPrev2;
  String bmPrev3;
  String bmPrev4;
  String bmPrev5;
}
