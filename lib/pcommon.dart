library bible.pcommon;

import 'package:bible_multi_the_life/penums.dart';
import 'package:bible_multi_the_life/pstyle.dart';
import 'package:bible_multi_the_life/resource.dart' as R;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:share_plus/share_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart' as URL;

//_snapBuild: 37;
bool isDebug = false;
int debugCounter = 0;
const int historyLimit = 5;
const int searchQueryLimit = 3;
const int pageRowLimit = 20;
const int bookmarkIdDevLimit = 1000;

///To update for each Release: AndroidManifest + pubspec.yaml + DbHelper + android/app/build.gradle
///Get versionNumber, versionDate, appOtherUrls
Map getVersion() {
  final String versionDate = "20221022"; //Manifest Version Code
  final String versionNumber = "1.22.0"; //Manifest version Name
  /* Droïd */
  const String appOtherUrls = "* Google:\nhttps://play.google.com/store/apps/details?id=org.hlwd.bible_multi_the_life \n\n* F-Droid:\nhttps://f-droid.org/en/packages/org.hlwd.bible_multi_the_life \n\n* Linux:\nhttps://snapcraft.io/bible-multi-the-life \n\n";
  /* Apple */
  //const String appOtherUrls = ""; //Let empty for Apple

  return {
    'versionNumber': versionNumber,
    'versionDate': versionDate,
    'appOtherUrls': appOtherUrls,
  };
}

Future<ThemeData> getThemeData() async {
  try {
    final String themeName = await Prefs.getThemeName;
    if (themeName.compareTo('DARK') == 0) return Future.value(PStyle.instance.darkThemeData);
  } catch (ex) {
    if (isDebug) print(ex);
  }

  return Future.value(PStyle.instance.lightThemeData);
}

/*
/// Method to slow code and test async methods
void slow() {
  sleep(Duration(milliseconds: 2000));
}
*/

/// Copy text to clipboard
/// [toastMsg] is displayed in toast if not null
Future<void> copyTextToClipboard(final BuildContext context, final String textToCopy, final String toastMsg, final bool shouldShowToast) async {
  try {
    await Clipboard.setData(ClipboardData(text: textToCopy));

    if (toastMsg != null) {
      if (shouldShowToast) showToast(context, toastMsg, Toast.LENGTH_SHORT);
    }
  } catch (ex) {
    if (isDebug) print(ex);
  }
}

Future<void> share(final String textToShare) async {
  try {
    await Share.share(textToShare.isEmpty ? "Clipboard is empty :)" : textToShare);
  } catch (ex) {
    if (isDebug) print(ex);
  }
}

void showToast(final BuildContext context, final String toastMsg, final Duration toastDuration) {
  try {
    toast(toastMsg, duration: toastDuration, context: context);
  } catch(ex) {
    if (isDebug) print(ex);
  }
}

Future<void> launchUrl(final dynamic url) async {
  try {
    Uri uri;
    if (url is String) {
      uri = Uri.parse(url);
    } else {
      uri = url;
    }

    if (await URL.canLaunchUrl(uri)) {
      await URL.launchUrl(uri);
    }
  } catch (ex) {
    if (isDebug) print(ex);
  }
}

Future<void> launchEmail({@required final String email, @required final String subject}) async {
  String encodeQueryParameters(Map<String, String> params) {
    return params.entries
        .map((e) => '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}')
        .join('&');
  }

  final Uri emailLaunchUri = Uri(
    scheme: 'mailto',
    path: email,
    query: encodeQueryParameters(<String, String>{
      'subject': subject
    }),
  );

  launchUrl(emailLaunchUri);
}

String nowYYYYMMDD() => DateFormat("yyyyMMdd").format(DateTime.now());

String replaceCustomHtml(String html) {
  try {
    html = html.replaceAll("<HS>", "<br><span><u>").replaceAll("</HS>", "</u></span>").replaceAll("<H>", "<h1><u>").replaceAll("</H>", "</u></h1>");
  } catch (ex) {
    if (isDebug) print(ex);
  }
  return html;
}

String rq(String str) {
  try {
    if (str == null) str = "";
    str = str.replaceAll("'", "''");
  } catch (ex) {
    if (isDebug) print(ex);
  }
  return str;
}

/// Preferences
abstract class AppPrefKey {
  static const BIBLE_NAME = 'BIBLE_NAME';
  static const BIBLE_LOCALE = 'BIBLE_LOCALE';
  static const BIBLE_ALT_LOCALE = 'BIBLE_ALT_LOCALE';
  static const THEME_NAME = 'THEME_NAME';
  static const BOOK_NUMBER = 'BOOK_NUMBER';
  static const CHAPTER_NUMBER = 'CHAPTER_NUMBER';
  static const FONT_NAME = 'FONT_NAME';
  static const FONT_SIZE = 'FONT_SIZE';
  static const TAB_SELECTED = 'TAB_SELECTED';
  static const CLIPBOARD_IDS = 'CLIPBOARD_IDS';
  static const LAYOUT_DYNAMIC1 = "LAYOUT_DYNAMIC1";
  static const LAYOUT_DYNAMIC2 = "LAYOUT_DYNAMIC2";
  static const LAYOUT_DYNAMIC3 = "LAYOUT_DYNAMIC3";
  static const LAYOUT_DYNAMIC4 = "LAYOUT_DYNAMIC4";
  static const LAYOUT_DYNAMIC5 = "LAYOUT_DYNAMIC5";
  static const LAYOUT_DYNAMIC6 = "LAYOUT_DYNAMIC6";
  static const LAYOUT_DYNAMIC7 = "LAYOUT_DYNAMIC7";
  static const STYLE_HIGHLIGHT_SEARCH = 'STYLE_HIGHLIGHT_SEARCH';
}

String convertLocaleToString(final BBLocale bbLocale) {
  try {
    final String localeStr = bbLocale.toString().toUpperCase();
    final int len = localeStr.length;
    if (len < 2) return 'EN';
    return localeStr.substring(len - 2, len);
  } catch (ex) {
    if (isDebug) print(ex);
  }
  return 'EN';
}

BBLocale convertStringToBBLocale(String localeAsString) {
  localeAsString = localeAsString.toUpperCase();
  switch (localeAsString) {
    case 'EN':
      return BBLocale.EN;
    case 'ES':
      return BBLocale.ES;
    case 'PT':
      return BBLocale.PT;
    case 'FR':
      return BBLocale.FR;
    case 'IT':
      return BBLocale.IT;
    case 'DE':
      return BBLocale.DE;
    default:
      return BBLocale.EN;
  }
}

/// Returns "" if alternate locale is not applicable
Future<String> getAlternateLocaleString() async {
  final String bbLocale = await Prefs.getBibleLocale;
  if (bbLocale == "DE") {
    return await Prefs.getBibleAltLocale;
  }
  return "";
}

String getBibleName(final String bbName0, final bool isVeryVerboseVersion) {
  switch (bbName0) {
    case "k":
      return isVeryVerboseVersion ? "King James 1611" : "(EN) KJV 1611";
    case "2":
      return isVeryVerboseVersion ? "King James 2000" : "(EN) KJV 2000";
    case "v":
      return isVeryVerboseVersion ? "Reina Valera" : "(ES) Valera";
    case "l":
      return isVeryVerboseVersion ? "Louis Segond" : "(FR) Segond";
    case "o":
      return isVeryVerboseVersion ? "Ostervald" : "(FR) Ostervald";
    case "d":
      return isVeryVerboseVersion ? "G. Diodati" : "(IT) Diodati";
    case "a":
      return isVeryVerboseVersion ? "Almeida" : "(PT) Almeida";
    case "s":
      return isVeryVerboseVersion ? "Schlachter" : "(DE) Schlachter";
    default:
      return '';
  }
}

String getLocaleNameVerbose(final String bbLocale) {
  switch (bbLocale.toUpperCase()) {
    case "EN":
      return "English";
    case "ES":
      return "Española";
    case "FR":
      return "Français";
    case "IT":
      return "Italiano";
    case "PT":
      return "Português";
    case "DE":
      return "Deutsch";
    default:
      return '';
  }
}

String getLocaleName(final int indexSelected) {
  final List<String> lstLocale = [
    "EN",
    "en",
    "ES",
    "FR",
    "fr",
    "IT",
    "PT",
    "DE"
  ];
  return (indexSelected >= 0 && indexSelected <= 7) ? lstLocale[indexSelected] : "EN";
}

void onShowDialog({@required BuildContext context, @required final R.id titleId, @required final R.id contentId, @required final TextStyle textStyle}) {
  showDialog(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext context) {
      return WillPopScope(
        onWillPop: () async => true,
        child: AlertDialog(
          scrollable: true,
          title: Text(R.getString(titleId)),
          content: Text(R.getString(contentId), style: textStyle, textAlign: TextAlign.left),
          actions: [
            OutlinedButton(
              onPressed: () => Navigator.pop(context),
              child: Text(R.getString(R.id.mnuClose)),
            ),
          ],
        ),
      );
    },
  );
}

class _PreferencesHelper {
  //Helper
  static Future<SharedPreferences> get prefs => SharedPreferences.getInstance();

  static Future<String> getString(String appPrefKey) async {
    final p = await prefs;
    return p.getString(appPrefKey) ?? '';
  }

  static Future<List<String>> getStringList(String appPrefKey) async {
    final p = await prefs;
    return p.getStringList(appPrefKey) ?? [];
  }

  static Future setString(String appPrefKey, String value) async {
    final p = await prefs;
    return p.setString(appPrefKey, value);
  }

  static Future setStringList(String appPrefKey, List<String> value) async {
    final p = await prefs;
    return p.setStringList(appPrefKey, value);
  }
}

class Prefs {
  static Future<String> get getBibleLocale => _PreferencesHelper.getString(AppPrefKey.BIBLE_LOCALE);
  static Future saveBibleLocale(String value) => _PreferencesHelper.setString(AppPrefKey.BIBLE_LOCALE, value);

  static Future<String> get getBibleAltLocale => _PreferencesHelper.getString(AppPrefKey.BIBLE_ALT_LOCALE);
  static Future saveBibleAltLocale(String value) => _PreferencesHelper.setString(AppPrefKey.BIBLE_ALT_LOCALE, value);

  static Future<String> get getBibleName => _PreferencesHelper.getString(AppPrefKey.BIBLE_NAME);
  static Future saveBibleName(String value) => _PreferencesHelper.setString(AppPrefKey.BIBLE_NAME, value);

  static Future<String> get getThemeName => _PreferencesHelper.getString(AppPrefKey.THEME_NAME);
  static Future saveThemeName(String value) => _PreferencesHelper.setString(AppPrefKey.THEME_NAME, value);

  static Future<String> get getFontName => _PreferencesHelper.getString(AppPrefKey.FONT_NAME);
  static Future saveFontName(String value) => _PreferencesHelper.setString(AppPrefKey.FONT_NAME, value);

  static Future<String> get getFontSize => _PreferencesHelper.getString(AppPrefKey.FONT_SIZE);
  static Future saveFontSize(String value) => _PreferencesHelper.setString(AppPrefKey.FONT_SIZE, value);

  static Future<String> get getLayoutDynamic1 => _PreferencesHelper.getString(AppPrefKey.LAYOUT_DYNAMIC1);
  static Future saveLayoutDynamic1(String value) => _PreferencesHelper.setString(AppPrefKey.LAYOUT_DYNAMIC1, value);

  static Future<String> get getLayoutDynamic2 => _PreferencesHelper.getString(AppPrefKey.LAYOUT_DYNAMIC2);
  static Future saveLayoutDynamic2(String value) => _PreferencesHelper.setString(AppPrefKey.LAYOUT_DYNAMIC2, value);

  static Future<String> get getLayoutDynamic3 => _PreferencesHelper.getString(AppPrefKey.LAYOUT_DYNAMIC3);
  static Future saveLayoutDynamic3(String value) => _PreferencesHelper.setString(AppPrefKey.LAYOUT_DYNAMIC3, value);

  static Future<String> get getLayoutDynamic4 => _PreferencesHelper.getString(AppPrefKey.LAYOUT_DYNAMIC4);
  static Future saveLayoutDynamic4(String value) => _PreferencesHelper.setString(AppPrefKey.LAYOUT_DYNAMIC4, value);

  static Future<String> get getLayoutDynamic5 => _PreferencesHelper.getString(AppPrefKey.LAYOUT_DYNAMIC5);
  static Future saveLayoutDynamic5(String value) => _PreferencesHelper.setString(AppPrefKey.LAYOUT_DYNAMIC5, value);

  static Future<String> get getLayoutDynamic6 => _PreferencesHelper.getString(AppPrefKey.LAYOUT_DYNAMIC6);
  static Future saveLayoutDynamic6(String value) => _PreferencesHelper.setString(AppPrefKey.LAYOUT_DYNAMIC6, value);

  static Future<String> get getLayoutDynamic7 => _PreferencesHelper.getString(AppPrefKey.LAYOUT_DYNAMIC7);
  static Future saveLayoutDynamic7(String value) => _PreferencesHelper.setString(AppPrefKey.LAYOUT_DYNAMIC7, value);

  static Future<String> get getTabSelected => _PreferencesHelper.getString(AppPrefKey.TAB_SELECTED);
  static Future saveTabSelected(String value) => _PreferencesHelper.setString(AppPrefKey.TAB_SELECTED, value);

  static Future<List<String>> get getClipboardIds => _PreferencesHelper.getStringList(AppPrefKey.CLIPBOARD_IDS);
  static Future saveClipboardIds(List<String> value) => _PreferencesHelper.setStringList(AppPrefKey.CLIPBOARD_IDS, value);

  static Future<String> get getStyleHighlightSearch => _PreferencesHelper.getString(AppPrefKey.STYLE_HIGHLIGHT_SEARCH);
  static Future saveStyleHighlightSearch(String value) => _PreferencesHelper.setString(AppPrefKey.STYLE_HIGHLIGHT_SEARCH, value);
}

//HIVE
/* PROBLEM WITH HIVE.
//Need to create it, remove all close, change savePref

Box box;
Future<bool> openBox() async {
  var dir = await getApplicationDocumentsDirectory();
  Hive.init(dir.path);

  box = await Hive.openBox('AppPref');
  return Future.value(true);
}

String getPref(final String appPrefKey) {
  //Box box;
  try {
    final String value = box.containsKey(appPrefKey) ? box.get(appPrefKey) : '';
    return value;
  } catch (ex) {
    if (isDebug) print(ex);
  } finally {
    try {
      box.close();
    } catch (ex) {
      if (isDebug) print(ex);
    }
  }
  return '';
}

/// Get preference
/// @return String
String _getPref(final String appPrefKey) {
  Box box;
  try {
    box = Hive.box('AppPref');
    final String value = box.containsKey(appPrefKey) ? box.get(appPrefKey) : '';
    return value;
  } catch (ex) {
    if (isDebug) print(ex);
  } finally {
    try {
      box.close();
    } catch (ex) {
      if (isDebug) print(ex);
    }
  }
  return '';
}

/// Save preference as string
void savePref(final String appPrefKey, String value) {
  Box box;
  try {
    if (value == null) value = '';
    box = Hive.box('AppPref');
    box.put(appPrefKey, value);
  } catch (ex) {
    if (isDebug) print(ex);
  } finally {
    try {
      box.close();
    } catch (ex) {
      if (isDebug) print(ex);
    }
  }
}
*/
